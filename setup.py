
from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='rearchiving',
    description='This is a repo for working on the s4m re-archiving process.',
    long_description=readme,
    author='Isaac Virshup',
    url="https://bitbucket.org/ivirshup/s4m-rearchiving"
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
