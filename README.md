# s4m re-archiving

This is a repo for working on the s4m re-archiving process.

The structure of this repo is copied whole-sale from [Kenneth Reitz sample python repo](https://github.com/kennethreitz/samplemod).

[Learn more](http://www.kennethreitz.org/essays/repository-structure-and-python).

This project has only been tested using Python 3.6 and will probably only run in Python 3.6 due to use of *f-string* syntax (which my pinkies are a fan of).

*Note*: I've used [git-lfs](https://git-lfs.github.com) ([atlassian guide](https://www.atlassian.com/git/tutorials/git-lfs)) to store the test date for this repository, so if you want the tests to work you're gonna have it installed.

## Scripts

Scripts in here are specifically to be used for re-archiving and de-archiving on pipe using our tape archives. Some might rely on server state (a file in a certain place, DMF acting the way it did that week, etc.). Use at your own risk. I've tried to document them (`python3 {scriptname.py} -h` style.)

## example

```python
from rearchiving import Dataset
from pathlib import Path
d = Dataset("./7115.tar")
new_dset = Path("./7115")
new_dset.mkdir()
d2 = d.reformat_dataset(new_dset) # Place reformatted dataset in new directory.
```


# Working thoughts:

These are some thoughts on modifications I've made or am considering making to this package.

## Dealing with symlinks:

*resolved with paths.py module, ability to resolve symlinks*

### What is bad:

* Nested symlinks (links containing links) can make it easier to pull
* Symlinks pointing outside the dataset (includes absolute paths which refere to things like `/nfs`)
* Symlinks that don't actually point to anything
* Links that look like: `'../tophat_out/../tophat_out/C11d0_1_1/C11d0_1_1.fastq.out/accepted_hits.bam/../tophat_out/C11d0_1_1/C11d0_1_1.fastq.out/accepted_hits.bam.fastq.out/accepted_hits.bam'`

### Possible solutions:

* Deal with symlinks that don't point to anything by making sure all symlinks point to something
	* If they are absolute, truncate
	* If they aren't
* Deal with 'absolute' links by truncating them

## Categorizing files

*This has gone through a lot of work. See methods for `Dataset._collect_{aligned, readfiles, metadata}` for current implementation.*

I noticed a few bugs in how I categorize files in the dataset and am reworking that now. I would like to get a little input on how exactly I should do this, and if there should be some change in methodology for datasets.

Here's an example of where I'm running into trouble:

* `D#6686` contains a file 	`6686/source/raw/download.zip` which is about 30 gb. I think this should go into the readfile archive.
* `D#7065` contains a file `7065/source/raw/Sequencing_Project_SusanNilsson_07-Oct-2016/FastQC_reports_for_Project_SusanNilsson_07-Oct-2016/WT_BM3_4_S223_L006_R1_001_fastqc.zip`, which probably shouldn't go in raw read files.

Another example:

* `6496/source/raw/trimmed/md5sums.FASTQ.gz`, without looking at the size or file name it'd be hard to tell what this is.

I think it could be a bit of a rabbit hole to write rules which pick up these kinds of discrepancies (both are under `source/raw` and end with `.zip`) from the name. Is there just a certain amount of error we should expect in categorization with file names? Should I implement a size based filter?

I worry implementing complicated rules for this process will just make it hard to deal with down the road.

A possible solution to this is keep simple rules for now, allow some error, but also allow a config file in datasets which allows placement of files in a specific archive. Probably a YAML file.

## Make modifications in memory, add `save`/ `write` command to make modifications.

Right now there is some ambiguity here. `dataset.reformat_dataset()` will modify the representation on disk in both the case dataset is a tar based, or file-system based. However, `dataset.resolve_symlinks()` will modify the filesystem on being called, while if you call it on a `tar` backed `Dataset` only the in memory values will be changed. If you then call `dataset.reformat_dataset()` the modified symlinks will be written (see test `test_symlinkres_tar`).

I would like to make this consistent.

I think it would be difficult to modify a tarfile in place, so I think this may be the best way to modify things.

Right now this only really affects symlink resolution, though I would also like to be able to move/ rename files from this interface.

### Thoughts on implementation

This would be implemented by having an `_initial` value for all members, so I have a pointer to the initial data. I would probably have to restrict what kind of data can be changed through this interface (for example, no contents of files). I'd have to be sure someone doesn't want to write a TarFile to it's current location.
