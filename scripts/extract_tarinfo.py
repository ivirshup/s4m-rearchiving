import pickle
from tarfile import TarFile, TarInfo
from pathlib import Path

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description="Extracts tarfile.TarInfo from a tarfile and pickles it.")
    parser.add_argument("tar", help="Tar file to inspect.", type=Path)
    parser.add_argument("pkl", help="Path to write pickle to.", type=Path)

    args = parser.parse_args()

    a = TarFile(args.tar, "r")
    members = a.getmembers()
    pickle.dump(members, open(args.pkl, "wb"))

if __name__ == '__main__':
    main()
