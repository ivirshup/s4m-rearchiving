import context
from rearchiving import Dataset, dmf
from pathlib import Path
from itertools import chain

BASEINPTH = Path("/nfs/HSM/archive/rnaseq")
BASEOUTPTH = Path("/nfs/HSM/working/scanning_archs_tmpdir")

def write_manifests(dset, outdir):
    readfiles = dset._collect_readfiles()
    metadata = dset._collect_metadata()
    aligned = dset._collect_aligned()
    contents = {}
    contents.update(aligned)
    contents.update({"metadata": metadata, "readfiles": readfiles})
    for k,v in contents.items():
        gbsize = sum(x.mbsize / 1000 for x in v)
        write_manifest(v, outdir.joinpath(f"{k}_{gbsize}gb.txt"))

def write_manifest(members, outpth):
    members = sorted(members, key=lambda x: x.size)
    with outpth.open("w") as f:
        f.write("name\tmbsize\n")
        for m in members:
            f.write(f"{m.name}\t{m.mbsize}\n")
        f.flush()

def reformat_dataset(pth):
    dset_id = pth.stem
    d = Dataset(pth, name = dset_id)
    d.resolve_symlinks()
    new_pth = BASEOUTPTH.joinpath(dset_id)
    new_pth.mkdir(exist_ok=True)
    print(f"Reformatting {dset_id}, writing to: {new_pth}")
    new_dataset = d.reformat_dataset(new_pth)
    print(f"Done reformatting {dset_id}.")
    write_manifests(d, new_pth)
    print(f"Removing temporary file {str(pth)}")
    dmf.remove_file(pth)
    print(f"Moving {dset_id} to tape.")
    dmf.move_to_tape(*[x.name for x in new_dataset.archives])
    print("Done.")
    return new_dataset

def main():
    import argparse
    parser = argparse.ArgumentParser(description="""Reformat passed archives.

    Archives will be copied from /nfs/HSM/archive/rnaseq/ to /nfs/HSM/working/scanning_archs_tmpdir/ then reformatted.
    """)
    parser.add_argument("archives", nargs="*", help="Filenames of archive to reformat.")

    args = parser.parse_args()
    archives = [BASEINPTH.joinpath(x) for x in args.archives]
    dmf.process_archives2(reformat_dataset, archives, tmpdir=BASEOUTPTH)
    # dmf.process_archives(reformat_dataset, archives, tmpdir=BASEOUTPTH)

if __name__ == '__main__':
    main()
