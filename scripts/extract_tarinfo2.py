import json
# from .context.rearchiving.util import extract_tarinfo
from tarfile import TarFile, TarInfo
import tarfile
from itertools import repeat
from pathlib import Path

def extract_tarinfo(tar, outtype=dict):
    """
    Given a tarfile, will extract all metadata of it's contents.

    Args:
        tar (TarFile, list{TarInfo}):
            TarFile or TarInfo types to extract from
        outtype (Type{dict}, Type{TarInfo}):
            What type you want returned, a little extra processing is done on
            `dict`s so they can be written to json.
    """
    if isinstance(tar, TarFile):
        info = tar.getmembers()
    else:
        info = tar
    type_table = []
    type_table.extend(zip(tarfile.REGULAR_TYPES, repeat("REGTYPE")) )
    type_table.append((tarfile.LNKTYPE, "LNKTYPE"))
    type_table.append((tarfile.SYMTYPE, "SYMTYPE"))
    type_table.append((tarfile.CHRTYPE, "CHRTYPE"))
    type_table.append((tarfile.BLKTYPE, "BLKTYPE"))
    type_table.append((tarfile.DIRTYPE, "DIRTYPE"))
    type_table.append((tarfile.FIFOTYPE, "FIFOTYPE"))
    type_table = dict(type_table)
    if outtype is dict:
        formatted = []
        for i in info:
            d = i.get_info()
            d["type"] = type_table[d["type"]]
            for k,v in d.items(): # Convert bytes to string
                if isinstance(v, bytes):
                    d[k] = v.decode("utf-8")
            formatted.append(d)
        return formatted
    else:
        return info

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description="Extracts tarfile.TarInfo from a tarfile and outputs it as JSON.")
    parser.add_argument("tar", help="Tar file to inspect.", type=Path)
    parser.add_argument("json", help="Path to write json to.", type=Path)

    args = parser.parse_args()

    info = extract_tarinfo(TarFile.open(args.tar, "r"))
    json.dump(info, open(args.json, "w"))

if __name__ == '__main__':
    main()
