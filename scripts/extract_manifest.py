import context
from tarfile import TarFile
from rearchiving.util import extract_manifest
import json
from rearchiving import Dataset
from pathlib import Path


def main():
    from argparse import ArgumentParser, FileType

    parser = ArgumentParser(description="Given an dataset, generate a json manifest")
    parser.add_argument("inputpth", type=Path, help="Input dataset (can be directory or archive).")
    parser.add_argument("outputpth", type=FileType("w"), help="Output json.")
    parser.add_argument("--pp", action="store_true", help="Enable pretty-printing of json file.")

    args = parser.parse_args()

    d = Dataset(args.inputpth)
    manifest = d.extract_manifest()

    with args.outputpth as f:
        if args.pp:
            json.dump(manifest, f, indent=1)
        else:
            json.dump(manifest, f)


if __name__ == '__main__':
    main()
