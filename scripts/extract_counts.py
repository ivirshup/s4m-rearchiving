import context
from rearchiving import Dataset, dmf
from pathlib import Path
from itertools import chain

BASEINPTH = Path("/nfs/HSM/archive/rnaseq")
BASEOUTPTH = Path("/nfs/HSM/working/extract_counts")


def write_manifests(dset, outdir):
    readfiles = dset._collect_readfiles()
    metadata = dset._collect_metadata()
    aligned = dset._collect_aligned()
    contents = {}
    contents.update(aligned)
    contents.update({"metadata": metadata, "readfiles": readfiles})
    for k, v in contents.items():
        gbsize = sum(x.mbsize / 1000 for x in v)
        write_manifest(v, outdir.joinpath(f"{k}_{gbsize}gb.txt"))


def write_manifest(members, outpth):
    members = sorted(members, key=lambda x: x.size)
    with outpth.open("w") as f:
        f.write("name\tmbsize\n")
        for m in members:
            f.write(f"{m.name}\t{m.mbsize}\n")
        f.flush()


def extract_counts(pth):
    dset_id = pth.stem
    d = Dataset(pth, name=dset_id)
    # d.resolve_symlinks() # Might not be necessary here
    new_pth = BASEOUTPTH.joinpath(dset_id)
    new_pth.mkdir(exist_ok=True)
    to_extract = [x for x in d.members if (
        x.isfile() and ("count_frags.txt" in x.name))]
    write_manifests(d, new_pth)
    print(f"Extracting from {dset_id}, writing to: {new_pth}")
    for f in to_extract:
        new_fpth = BASEOUTPTH.joinpath(f.name)
        if not new_fpth.parent.exists():
            new_fpth.parent.mkdir(parents=True)
        with f.open() as fr, new_fpth.open("wb") as to:
            to.write(fr.read())
    print(f"Done extracting counts from {dset_id}.")
    # Only delete if we're sure we're not deleting the real thing
    if ("archive" not in str(pth)) and (Path("/nfs/HSM/archive/rnaseq") / pth.name).exists():
        dmf.remove_file(pth)
    print("Done.")
    return to_extract


def main():
    import argparse
    parser = argparse.ArgumentParser(description="""Extract counts from passed archives.

    Archives will be copied from /nfs/HSM/archive/rnaseq/ to /nfs/HSM/working/scanning_archs_tmpdir/ then reformatted.
    """)
    parser.add_argument("archives", nargs="*",
                        help="Filenames of archive to reformat.")
    parser.add_argument("-n", "--n_procs", default=5, type=int,
        help="Number of archive to process at once (default: 5).")

    args = parser.parse_args()
    archives = [BASEINPTH.joinpath(x) for x in args.archives]
    dmf.process_archives2(extract_counts, archives, tmpdir=BASEOUTPTH, n_concurrent=args.n_procs)


if __name__ == '__main__':
    main()
