from subprocess import run, PIPE, Popen
from tarfile import TarFile, TarInfo
import tarfile
from itertools import repeat
from pathlib import Path
import os
from time import sleep
from collections import namedtuple
from multiprocessing import Pool

def init_copy_from_tape(inpth, outpth):
    """Return open process for copying file from tape to disk."""
    return Popen(["dmcopy", str(inpth), str(outpth)])

def init_copy_from_disk(inpth, outpth):
    """Return open process for copying file from disk to disk"""
    return Popen(["cp", str(inpth), str(outpth)])

def init_copy_archive(inpth, outpth):
    """
    Copy tar archive from `inpth` to `outpth`.

    If `inpth` is managed by DMF,
    """
    inpth, outpth = Path(inpth), Path(outpth)
    assert inpth.exists(), f"{inpth} does not exist."
    status = check_status(inpth)
    if status == "OFL":
        proc = init_copy_from_tape(inpth, outpth)
    elif status in ["MIG", "REG", "DUL"]:
        proc = init_copy_from_disk(inpth, outpth)
    return proc

def copy_from_tape(inpth, outpth):
    """Blocking call to copy archive to disk."""
    return run(["dmcopy", str(inpth), str(outpth)], check=True)

def copy_from_disk(inpth, outpth):
    """Returns open process for copying file from disk to disk"""
    return run(["cp", str(inpth), str(outpth)], check=True)

def copy_archive(inpth, outpth):
    """
    Copies tar archive from `inpth` to `outpth`.

    If `inpth` is managed by DMF,
    """
    inpth, outpth = Path(inpth), Path(outpth)
    assert inpth.exists(), f"{inpth} does not exist."
    status = check_status(inpth)
    if status == "OFL":
        proc = copy_from_tape(inpth, outpth)
    elif status in ["MIG", "REG", "DUL"]:
        proc = copy_from_disk(inpth, outpth)
    return proc

def move_to_tape(*files):
    """Push file to tape in a blocking call."""
    return run(["dmput", "-w", *[str(f) for f in files]], check=True)

def remove_file(pth):
    """Remove file from disk, checking that it's not a canonical archive."""
    assert "archive" not in str(pth.absolute()), f"Path was {str(pth.absolute())}"
    os.remove(pth)

def check_status(pth):  # Should this return an enum?
    """Check status of file under DMF system, returning string."""
    proc = run(["dmattr", "-a", "state", str(pth)], check=True, stdout=PIPE)
    return proc.stdout.decode().rstrip("\n")

#TODO figure out how to remove tmp file via argument to this function
def process_archives2(process, archives, tmpdir, n_concurrent=5):
    """
    Runs `process` over all `archives`.

    Runs a read-only process over the archives. No changes will be made to the
    tape copy of the archive.

    Copies each archive to disk under `tmpdir`, then calls `process` on the
    resulting path. After `process` is done, this function will not remove 
    temporary file, that is left to the process.

    Args:
        process (function):
            Function called on each archive.
        archives (iterable of str or Path):
            Archives to run process on.
        tmpdir (str, Path):
            Directory to copy archives to.
        n_concurrent (int):
            Number of archives that can be copied at one time.
    """
    archives = [Path(x) for x in archives]
    archives.sort(key=lambda x: x.stat().st_size)
    tmpdir = Path(tmpdir)
    global func  # TODO fix this gross thing
    def func(archive):
        tmppth = tmpdir.joinpath(archive.name)
        print(f"Starting to copy {str(archive)} to {str(tmppth)}.")
        copy_archive(archive, tmppth)
        print(f"Finished copy of {archive.name}.")
        print(f"Calling process on {archive.name}.")
        process(tmppth)
        print(f"Done processing {archive.name}.")
    with Pool(processes=n_concurrent) as p:
        p.map(func, archives)

def process_archives(process, archives, tmpdir, n_concurrent=5):
    """
    Runs `process` over all `archives`.

    Runs a read-only process over the archives. No changes will be made to the
    tape copy of the archive.

    Copies each archive to disk under `tmpdir`, then calls `process` on the
    resulting path. After `process` is done, will delete the copy of the tar.

    Args:
        process (function):
            The function which should be called on each archive. # Archives as tarfiles or Dataset?
        archives (iterable of str or Path):
            Archives to run process on.
        tmpdir (str, Path):
            Directory to copy archives to.
        n_concurrent (int):
            Number of archives that can be copied at one time.
    """
    Archive = namedtuple("Archive", ("archive_pth", "disk_pth", "process"))
    archives = [Path(x) for x in archives]
    tmpdir = Path(tmpdir)
    archives.sort(key=lambda x: x.stat().st_size)
    to_read = []
    read = []
    reading = []
    for a in archives:
        to_read.append(
            Archive(archive_pth=a, disk_pth=tmpdir.joinpath(a.name), process=None)
        )
    while len(to_read) + len(reading) > 0:
        to_remove = []
        for archive in reading:
            archive.process.poll()
            if archive.process.returncode == 0:
                print(f"{archive.disk_pth} has finished writing! Extracting files...")
                process(archive.disk_pth)
                to_remove.append(archive)
            elif archive.process.returncode is not None:
                for archive in reading:
                    archive.process.kill()  # Kill all running processes.
                raise Exception(f"uhoh, {archive} returned poorly. Returned: {archive.process.returncode}")
                # reading.remove(archive) # Do I want to continue?
        for archive in to_remove:
            print(f"Removing disk file {archive.disk_pth}.")
            remove_file(archive.disk_pth)
            read.append(archive)
            reading.remove(archive)
        while (len(reading) < n_concurrent) & (len(to_read) > 0):
            archive = to_read.pop()
            print(f"Adding {archive.archive_pth} to copy queue")
            archive = archive._replace(process=init_copy_archive(archive.archive_pth, archive.disk_pth))
            reading.append(archive)
        sleep(10)
