import tarfile
from tarfile import TarFile
from itertools import repeat, chain
from hashlib import md5
from pathlib import Path

TARTYPETABLE = []
TARTYPETABLE.extend(zip(tarfile.REGULAR_TYPES, repeat("REGTYPE")))
TARTYPETABLE.append((tarfile.LNKTYPE, "LNKTYPE"))
TARTYPETABLE.append((tarfile.SYMTYPE, "SYMTYPE"))
TARTYPETABLE.append((tarfile.CHRTYPE, "CHRTYPE"))
TARTYPETABLE.append((tarfile.BLKTYPE, "BLKTYPE"))
TARTYPETABLE.append((tarfile.DIRTYPE, "DIRTYPE"))
TARTYPETABLE.append((tarfile.FIFOTYPE, "FIFOTYPE"))
TARTYPETABLE = dict(TARTYPETABLE)


def tarinfo_df(tarinfo):
    import pandas as pd
    df = pd.DataFrame({
        "Name": [x.name for x in tarinfo],
        "Size": [x.size for x in tarinfo]
    })
    return df


def tar_df(tar):
    """Given a tar file, create a dataframe of its file contents."""
    members = [x for x in tar.getmembers() if x.isfile()]
    df = tarinfo_df(members)
    return df


def show_tarinfo(lst, dirinfo=False):
    """Given a list of tarinfo objects, print them in a useful way."""
    lst = sorted(lst, key=lambda x: x.size, reverse=True)
    for i in lst:
        if dirinfo:
            n = i.name
        else:
            n = i.name.split("/")[-1]
        print(f"{n}\t{i.size / (1024*1024)}M")


def extract_tarinfo(tar, outtype=dict):
    """
    Given a tarfile, will extract all metadata of it's contents.

    Args:
        tar (TarFile, list{TarInfo}):
            TarFile or TarInfo types to extract from
        outtype (Type{dict}, Type{TarInfo}):
            What type you want returned, a little extra processing is done on
            `dict`s so they can be written to json.
    """
    if isinstance(tar, TarFile):
        info = tar.getmembers()
    else:
        info = tar
    if outtype is dict:
        formatted = []
        for i in info:
            d = i.get_info()
            d["type"] = TARTYPETABLE[d["type"]]
            for k,v in d.items(): # Convert bytes to string
                if isinstance(v, bytes):
                    d[k] = v.decode("utf-8")
            formatted.append(d)
        return formatted
    else:
        return info


def format_tarinfo(info):
    """Format a human-readable dict from TarInfo."""
    d = info.get_info()
    d["type"] = TARTYPETABLE[d["type"]]
    for k, v in d.items():  # Convert bytes to string
        if isinstance(v, bytes):
            d[k] = v.decode("utf-8")
    return d


def extract_manifest(archive):
    """
    Given an archive, return a manifest of its contents.

    This includes taking md5 sums of each file in the archive.
    """
    metadata = extract_tarinfo(archive, outtype=dict)
    metadata_files = (m for m in metadata if m["type"] == "REGTYPE")
    for m in metadata_files:
        with archive.extractfile(m["name"]) as f:
            hash_md5 = md5file(f)
        m["md5"] = hash_md5
    return metadata


def md5file(f):
    """Given a file-like-object, calculate md5sum."""
    hash_md5 = md5()
    for chunk in iter(lambda: f.read(4096), b""):
        hash_md5.update(chunk)
    return hash_md5.hexdigest()


def clean_dir(d):
    """
    Given a dataset directory, removes files we don't want.

    Files we don't want end in ".bak" or ".attic"
    Arg:
        d (Path, str):
            Directory to search.
    """
    d = Path(d)
    to_remove = chain(d.rglob("*.bak"), d.rglob("*.attic"))
    # Ordering so directory contents deleted before directories
    to_remove = sorted(to_remove, reverse=True)
    for f in to_remove:
        if f.is_file() | f.is_symlink():
            f.unlink()
        elif f.is_dir():
            f.rmdir()
