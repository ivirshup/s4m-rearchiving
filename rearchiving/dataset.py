from pathlib import Path
from tarfile import TarFile, TarInfo
from itertools import groupby, chain
import re
import os
import abc
from . import paths
from .paths import resolve_sym
from .util import extract_tarinfo, md5file, format_tarinfo

DIR_RE = re.compile(r"source/([^/]+)/")


class DatasetError(Exception):
    """Base error class for Datasets"""
    pass


class DatasetContentsError(DatasetError):
    """Exception raised for error in contents of a dataset."""
    def __init__(self, message):
        self.message = message


class UnreadableError(DatasetError):
    """Exception raised when a member of a dataset can't be read."""
    pass


class Member(abc.ABC, object):
    """
    Defining an interface for members of a dataset.

    The purpose of this superclass is to make sure I have a common interface for
    all members of the dataset, so the user doesn't have to call `.isfile` for
    TarFile members, and `.is_file` for file-system members.

    This uses a python Abstract Base Class to enforce a common interface for all
    subclasses. If any subclass does not define properties marked with
    `@abc.abstractproperty` or methods marked with `@abc.abstractmethod` it will
    throw an error at definition time.
    """

    def __repr__(self):
        return f"<{str(self.__class__)}: {self.name}>"

    @abc.abstractproperty
    def _base(self):
        """Return instance of base type underlying Member implementation."""

    @abc.abstractproperty
    def name(self):
        """Name in dataset, should be a path (probably of type str)"""

    @abc.abstractproperty
    def mode(self):
        """Permissions for member."""

    @mode.setter
    @abc.abstractproperty
    def mode(self):
        """Define a setter method for mode."""

    @abc.abstractproperty
    def linkname(self):
        """Should return an error if not a link, otherwise should be path to link."""

    @linkname.setter
    @abc.abstractproperty
    def linkname(self, newval):
        pass

    @abc.abstractmethod
    def __str__(self):
        pass

    @abc.abstractmethod
    def isdir(self):
        """True if self is a directory."""

    # @abc.abstractmethod # There are no hard links so far, also not clear if this is always resolvable
    # def islnk(self):
    #     """True if self is a hard link"""

    @abc.abstractmethod
    def issym(self):
        """True if self is a symlink"""

    @abc.abstractmethod
    def size(self):
        """Return size of self in bytes"""

    @property
    def mbsize(self):
        """Convencience property for returning size in mb."""
        return self.size / (1024*1024)

    @abc.abstractmethod
    def open(self):
        """Return read only file-like-object for file"""

    @abc.abstractmethod
    def get_info(self):
        """Return a representation of self as dict."""


class TarMember(Member):
    """Member of a dataset which is in a tarfile."""

    def __init__(self, dset, tarinfo):
        """
        Args:
            dset (Dataset):
                The dataset this belongs to.
            tarinfo (tarfile.TarInfo):
                Tar info to base method of off.
        """
        if not isinstance(dset, Dataset):
            raise TypeError("arg `dset` must be of type `Dataset`.")
        self.dataset = dset
        if not isinstance(tarinfo, TarInfo):
            raise TypeError("arg `tarinfo` must be of type `TarInfo.`")
        self._tarinfo = tarinfo

    def __str__(self):
        return self.name

    @property
    def _base(self):
        return self._tarinfo

    @property
    def name(self):
        name = self._tarinfo.name
        if self.isdir() & name.endswith("/"):
            name = name.rstrip("/")
        return name

    @property
    def mode(self):
        return self._tarinfo.mode

    @mode.setter
    def mode(self, newval):
        if isinstance(newval, str):
            newval = int(newval, base=8) # TODO
        elif not isinstance(newval, int):
            raise TypeError(f"Either and int or str is required, got {type(newval)}.")
        self._tarinfo.mode = newval

    @property
    def size(self):
        return self._tarinfo.size

    @property
    def linkname(self):
        if not self.issym():
            raise OSError("{self} is not a symbolic link.")
        return self._tarinfo.linkname

    @linkname.setter
    def linkname(self, newval):
        """
        Target of symlink.
        """
        curname = self.linkname  # Ensures that self has linkname
        validnames = self.dataset.get_valid_link_targets()
        resolved = resolve_sym(self.name, newval)
        assert resolved in validnames, "Link target does not exist."
        self._tarinfo.linkname = newval

    def isdir(self):
        return self._tarinfo.isdir()

    def issym(self):
        return self._tarinfo.issym()

    def isfile(self):
        return self._tarinfo.isfile()

    def islnk(self):
        return self._tarinfo.islnk()

    def open(self):
        """Return read only file-like-object for file from archive."""
        if not self.isfile():
            raise UnreadableError(f"{self.name} cannot be read, because it is not a file.")
        archive = self.dataset._find_archive(self)
        return archive.extractfile(self._base)

    def get_info(self):
        """Return a representation of self as dict."""
        info = format_tarinfo(self._tarinfo)
        if self.isdir() & info["name"].endswith("/"):
            info["name"] = info["name"].rstrip("/")
        return info

class FSMember(Member):
    """
    Member of a dataset which is in the filesystem.

    Internally this is a pathlib.Path instance, which should point to where-ever
    the relevant fileobject is on the system.
    """
    def __init__(self, dset, pth):
        """
        Args:
            dset (Dataset):
                The dataset this belongs to.
            pth (Path, str):
                Path to this file.
        """
        if not isinstance(dset, Dataset):
            raise TypeError("arg `dset` must be of type `Dataset`.")
        self.dataset = dset
        self._path = Path(pth)
        if not self._path.exists():
            if self.issym():
                raise FileNotFoundError(f"Could not find target of link.\nLink: {self.name}\nTarget: {self.linkname}.")
            else:
                raise FileNotFoundError(f"Could not find file: {self.name}.")

    def __str__(self):
        return self.name

    @property
    def _base(self):
        return self._path

    @property
    def name(self):
        dataset_dir = self.dataset.directory.absolute()
        abspath = self._path.absolute()
        relpath = abspath.relative_to(dataset_dir)
        name = str(relpath)
        if name == ".":
            return dataset_dir.name
        if self.isdir() & name.endswith("/"):
            name = name.rstrip("/")
        return f"{self.dataset.directory.name}/{name}"

    @property
    def mode(self):
        return self._path.lstat().st_mode

    @mode.setter
    def mode(self, newval):
        if isinstance(newval, str):
            newval = int(newval, base=8)
        elif not isinstance(newval, int):
            raise TypeError(f"Either and int or str is required, got {type(newval)}.")
        os.chmod(self._path, newval)

    @property
    def size(self):
        return os.path.getsize(self._path)

    @property
    def linkname(self):
        if not self.issym():
            raise OSError("{self} is not a symbolic link.")
        return os.readlink(self._path)

    @linkname.setter
    def linkname(self, newval):
        curname = self.linkname  # Ensures that self has linkname
        validnames = self.dataset.get_valid_link_targets()
        resolved = resolve_sym(self.name, newval)
        assert resolved in validnames, "Link target does not exist."
        oldpth = self._path
        newpth = Path(str(oldpth))
        oldpth.unlink()
        newpth.symlink_to(newval)
        self._path = newpth

    def isdir(self):
        return self._path.is_dir() and not self._path.is_symlink()

    def isfile(self):
        return self._path.is_file() and not self._path.is_symlink()

    def issym(self):
        return self._path.is_symlink()

    def open(self):
        """Return read only file-like-object for file from filesystem."""
        if not self.isfile():
            raise UnreadableError(f"{self.name} cannot be read, because it is not a file.")
        return self._base.open("rb")

    def get_info(self):
        """Return a representation of self as dict."""
        info = {}
        info["name"] = self.name
        info["size"] = self.size
        if self.issym():
            info["type"] = "SYMTYPE"
            info["linkname"] = self.linkname
        elif self.isfile():
            info["type"] = "REGTYPE"
        elif self.isdir():
            info["type"] = "DIRTYPE"
        else:
            raise DatasetError(f"Could not resolve type of {self}.")
        return info


class RecordMember(Member):
    def __init__(self, dset, record):
        self.dataset = dset
        if hasattr(record, "_asdict"):
            record = record._asdict()
        elif hasattr(record, "get_info"):
            record = record.get_info()
        assert isinstance(record, dict), "Only accepts dictionaries or things I can coerce to dictionaries a.t.m."
        # TODO validate fields
        if (record["type"] == "DIRTYPE") & (record["name"].endswith("/")):
            record["name"] = record["name"][:-1]
        self._record = record

    def __str__(self):
        return self.name

    @property
    def _base(self):
        return self._record

    @property
    def name(self):
        return self._record["name"]

    @property
    def mode(self):
        return self._record.get("mode", None)

    @mode.setter
    def mode(self, newval):
        if isinstance(newval, str):
            newval = int(newval, base=8)
        elif not isinstance(newval, int):
            raise TypeError(f"Either and int or str is required, got {type(newval)}.")
        self._record["mode"] = newval

    @property
    def size(self):
        return self._record["size"]

    @property
    def linkname(self):
        if not self.issym():
            raise OSError("{self} is not a symbolic link.")
        return self._record["linkname"]

    @linkname.setter
    def linkname(self, newval):
        curname = self.linkname  # Ensures that self has linkname
        validnames = self.dataset.get_valid_link_targets()
        resolved = resolve_sym(self.name, newval)
        assert resolved in validnames, "Link target does not exist."
        self._record["linkname"] = newval

    def isdir(self):
        return self._record["type"] == "DIRTYPE"

    def isfile(self):
        return self._record["type"] == "REGTYPE"

    def issym(self):
        return self._record["type"] == "SYMTYPE"

    def open(self):
        """Return read only file-like-object for file from filesystem."""
        raise UnreadableError(f"{self.name} cannot be read, because {self} is just a record.")

    def get_info(self):
        """Return a representation of self as dict."""
        return self._record

class Dataset(object):
    """
    Stemformatics dataset.
    """

    def __init__(self, initial=None, directory=".", name=""):
        self.members = []
        self.archives = []
        self._name = name
        if initial is not None:
            initial = Path(initial)
            if initial.is_dir():
                self.directory = initial  # Maybe add error here?
                self.scan_dir(initial)
            elif initial.is_file() & (initial.suffix == ".tar"):
                self.scan_archive(initial)
                self.directory = Path(directory)

    @property
    def name(self):  # TODO: Redo, probably by re-working instantiation
        if self._name == "":
            startswith = [f.name.split("/")[0] for f in self.members]
            name = startswith.pop()
            if all(x == name for x in startswith):
                self._name = name
        return self._name

    def __repr__(self):
        return "<rearchiving.Dataset '{}' with {} files and {} archives>".format(self.name, len(self.members), len(self.archives))

    def scan_dir(self, d):
        """
        Read contents of directory and add to self

        Args:
            d (str, Path):
                Directory to scan.

        Returns: nothing
        """
        d = Path(d)
        d_contents = d.rglob("*")
        for p in d_contents:
            if p.suffix == ".tar":
                self.scan_archive(p)
            else:
                self.members.append(FSMember(self, p))
        # If dataset is all on disk.
        try:
            self.findname(d.name)
        except AssertionError:
            self.members.append(FSMember(self, d))
            pass
        self.members.sort(key=lambda x: x.name)

    def scan_archive(self, a):
        """
        Read contents of a tar archive and add to self.

        Args:
            a (str, Path):
                Archive to scan.

        Returns: nothing
        """
        a = TarFile.open(a, "r")
        contents = a.getmembers()
        # a.close() # TODO: Reconsider this, I can't seem to open file again after closing it without creating new TarFile instance
        self.members.extend([TarMember(self, x) for x in contents])
        self.archives.append(a)

    def findname(self, name):
        """Given a name, returns Member in self with that name"""
        matching = [x for x in self.members if x.name == name]
        assert len(matching) == 1, f"Error: More than one member had that name: {matching}"
        return matching[0]

    def collect_symlinks(self):
        """Returns symlinks in Dataset as iterator."""
        return filter(lambda x: x.issym(), self.members)

    def _collect_aligned(self):
        keyfunc = lambda x: DIR_RE.search(x.name).group(1)
        viable_extensions = [
            ".sam",
            ".bam"
        ]
        viable_contents = [
            ".sam.",
            ".bam."
        ]
        files = filter(lambda x: not x.isdir(), self.members)
        files = filter(lambda x: any(x.name.endswith(ext) for ext in viable_extensions) |
                                 any(substr in x.name for substr in viable_contents)
                       , files)
        grouped = groupby(sorted(files, key=keyfunc), keyfunc)
        return {k: list(v) for k, v in grouped}

    def _collect_readfiles(self):
        viable_extensions = [
            ".fastq",
            ".fastq.gz",
            ".csfasta.gz",
            ".csfasta",
            ".QV.qual",
            ".sra"
        ]
        inraw_extensions = [
            ".zip",
            ".tar",
            ".tar.gz"
        ]
        readfiles = []
        for m in self.members:
            if any(m.name.endswith(x) for x in viable_extensions):
                readfiles.append(m)
            elif any(m.name.endswith(x) for x in inraw_extensions) and ("source/raw" in m.name):
                readfiles.append(m)
        return readfiles

    def _collect_metadata(self):  # What should this contain? symlinks/ directories?
        files = self.members
        read_files = self._collect_readfiles()
        aligned_files = list(chain.from_iterable(self._collect_aligned().values()))
        return [f for f in files if (f not in read_files) & (f not in aligned_files)]

    def reformat_dataset(self, newloc):
        """
        First take on how I'd like to reorganize datasets.
        """
        metadata = self._collect_metadata()
        fastq = self._collect_readfiles()
        aligned_groups = self._collect_aligned()
        assert set(self.members).issubset( set.union(set(metadata), set(fastq), set(chain.from_iterable(aligned_groups.values())))) # Have all files
        new_dataset = Dataset(directory=newloc)
        metadata_archive = create_archive("metadata", newloc, [x for x in metadata]) # TODO, enable having multiple archives
        metadata_archive.close()
        fastq_archive = create_archive("fastq", newloc, [x for x in fastq]) # TODO, enable having multiple archives
        fastq_archive.close()
        aligned_archives = []
        for group, aligned in aligned_groups.items():
            aligned_archive = create_archive(f"aligned_{group}", newloc, [x for x in aligned]) # TODO, enable having multiple archives
            aligned_archive.close()
            aligned_archives.append(aligned_archive)
        new_dataset.scan_archive(metadata_archive.name)
        new_dataset.scan_archive(fastq_archive.name)
        for aligned_archive in aligned_archives:
            new_dataset.scan_archive(aligned_archive.name)
        return new_dataset

    def get_valid_link_targets(self):
        """
        Get the names of valid link targets within this dataset.
        """
        validnames = [x.name for x in self.members if not x.issym()]
        readfiles = [x.name for x in self._collect_readfiles() if ("source/raw" in x.name) & x.name.endswith(".gz")]
        validnames.extend([x[:-3] for x in readfiles]) # Allowing symlinks to files which were compressed during archival process.
        return validnames

    def resolve_symlinks(self):
        """
        For each symlink member ensures that linkname points to a real file.

        Will make corrections, make sure no symlinks are nested and all targets
        exist.
        """
        links = list(self.collect_symlinks())
        resolvednames = paths.resolve_syms(links)
        fs_graph = paths.make_graph(self.members)
        for lnk, resolved in zip(links, resolvednames):
            try:
                target = self.findname(resolved)
                added_gz = False
            except AssertionError as e:
                if "source/raw" in resolved:
                    try:
                        target = self.findname(resolved + ".gz")
                        added_gz = True
                    except AssertionError:
                        raise e  # Re-raise previous error
                    pass
                else:
                    raise e
            relpath = paths.relative_path(fs_graph, lnk, target)
            relpath = paths.resolve_edges(relpath)
            if not added_gz:
                lnk.linkname = relpath
            else:
                lnk.linkname = relpath[:-3]  # Cut off ".gz"

    def validate_symlinks(self):
        """Check whether symlinks have real target."""
        validnames = self.get_valid_link_targets()
        links = list(self.collect_symlinks())
        resolvednames = paths.resolve_syms(links)
        badlinks = []
        for lnk, resolved in zip(links, resolvednames):
            if resolved not in validnames:
                badlinks.append((lnk, resolved))
        if len(badlinks) > 0:
            error_msg = "\n".join([f"Symlink '{lnk[0].name}' target could not found at {lnk[1]}." for lnk in badlinks])
            raise OSError("Found broken symlinks:\n" + error_msg)

    def is_valid(self):  # Should this return boolean or error?
        """Based on validate_rnaseq_dataset from Othmar's shell script."""
        # rnaseq validation
        valid = True
        readfiles = list(filter(lambda x: f"{self.name}/source/raw" in x.name, self._collect_readfiles())) # Should that just be a part of readfiles
        if len(inraw) < 1:
            Warning("Dataset does not contain any fastq files.")
            valid = False
        procdir_name = f"{self.name}/source/processed"
        try:
            procdir = self.findname(procdir_name) # I guess I need to have a name id
            if not procdir.issym():
                Warning("{procdir_name} was not symlink.")
                valid = False
        except AssertionError:
            Warning("Could not find file {procdir_name}")
            valid = False
            pass
        return valid

    def is_valid_s4m(self):
        """Check whether dataset is a valid s4m dataset."""
        accessionmember = self.findname(f"{self.name}/ACCESSION")
        accession = accessionmember.open().read().rstrip().decode("utf-8")
        should_exist = [
            f"{self.name}/ACCESSION",
            f"{self.name}/VALIDATED",
            f"{self.name}/source/raw",
            f"{self.name}/source/normalized",
            f"{self.name}/METASTORE",
            f"{self.name}/{accession}.gct" #  Should match contents of ACCESSION, but would have to read it. Might do?
        ]
        names = [x.name for x in self.members]
        for f in should_exist:
            if f not in names:
                raise DatasetContentsError(f"Couldn't find {f} in Dataset. Has it been completed?")

    def _find_archive(self, member):
        """
        If Dataset has archives, return archive containing requested member.
        """
        if isinstance(member, Member):
            membername = member.name
        elif isinstance(member, str) or isinstance(member, Path):
            membername = str(member)
        for a in self.archives:
            if membername in a.getnames():
                return a
        raise DatasetContentsError(f"Could not find {membername} in {self}.")

    def extract_manifests(self, md5=True):
        """Create a manifest for each archive in the dataset."""
        all_metadata = {}
        for a in self.archives:
            metadata = extract_tarinfo(a, outtype=dict)
            if md5:
                for m in (m for m in metadata if m["type"] == "REGTYPE"):
                    with a.extractfile(m["name"]) as f:
                        hash_md5 = md5file(f)
                    m["md5"] = hash_md5
            all_metadata[a.name] = metadata
        return all_metadata

    def extract_manifest(self, md5=True):
        """Create one manifest for the entire dataset."""
        manifest = []
        for m in self.members:
            info = m.get_info()
            if md5 & m.isfile():
                with m.open() as f:
                    info["md5"] = md5file(f)
            manifest.append(info)
        return manifest


def create_archive(name, loc, files):
    """Create new archive.

    Args:
        name (str):
            Name for new archive.
        loc (str, Path):
            Location for new archive.
        files (list{TarInfo, str, Path}):
            Files to place in new archive.
    """
    loc = Path(loc)
    files = list(files)
    assert loc.is_dir()
    newtar = TarFile(loc.joinpath(f"{name}.tar"), "w")
    for f in files:
        if isinstance(f, TarMember):
            if f.isfile():
                newtar.addfile(f._tarinfo, f.open())
            elif f.issym() | f.isdir():
                newtar.addfile(f._tarinfo)  # Where should links go?
        elif isinstance(f, FSMember):
            newtar.add(f._path, arcname=f.name, recursive=False)
        else:
            newtar.add(f._base, recursive=False)  # Should I just filter directories out of input?
    return newtar
