from pathlib import Path
import os.path
import networkx as nx

def findname(iterable, name):
    """
    Given a list of Members and one of their names, returns matching Member.

    Helper function.
    """
    matching = [x for x in iterable if x.name == name]
    assert len(matching) == 1, f"Couldn't find single matching name for {name}, found {len(matching)}."
    return matching[0]

def resolve_sym(sym, target):
    """Helper function to resolve where symlink is pointing to.

    Args:
        sym (str):
            Path to symlink.
        target (str):
            Relative path from symlink's directory to it's target.
    """
    sym = str(sym)
    target = str(target)
    symparent = sym[:sym.rfind("/")]
    return os.path.normpath(os.path.join(symparent, target))

def resolve_syms(links):
    """
    Given symlinks, will find out if any contain eachother, and resolve them.

    Should give true paths to all targets.
    """
    anychange = 1
    names = [x.name for x in links]
    resolved = [resolve_sym(x.name, x.linkname) for x in links]
    while anychange:  # To be sure we're not placing symlinks in the result
        anychange = 0
        for i in range(len(names)):
            target = resolved[i]
            target_parts = target.split("/")
            parentname = ""
            for p in target_parts:
                parentname += p
                if parentname in names:
                    anychange += 1
                    lnkidx = names.index(parentname)
                    parentname = resolved[lnkidx]
                parentname += "/"
            parentname = parentname[:-1]  # Remove final backslash
            resolved[i] = parentname
    return resolved

def relative_path(g, link, target):
    """Given a link and target in the filesystem, find connecting path."""
    linkname = link.name
    parentname = linkname[:linkname.rfind("/")]
    parent = findname(g.nodes(), parentname)
    path = nx.shortest_path(nx.Graph(g), parent, target)
    return path

def make_graph(members):
    """Constructs graph from members of a Dataset, leaves out symlinks."""
    g = nx.DiGraph()
    members = list(members)
    dirs = [x for x in members if x.isdir()]
    files = [x for x in members if x.isfile()]
    links = [x for x in members if x.issym()]
    dirs.sort(key=lambda x: len(x.name.split("/")))  # Ensures we add them in right order.
    for d in dirs:
        g.add_node(d)
        dls = d.name.split("/")
        if len(dls) > 1:
            parentname = "/".join(dls[:-1])
            parent = findname(dirs, parentname)
            g.add_edge(parent, d)
    for f in files:
        g.add_node(f)
        parentname = f.name[:f.name.rfind("/")]
        parent = findname(dirs, parentname)
        g.add_edge(parent, f)
    return g

def resolve_edges(path):
    """Given edges on graph of filesytem, returns relative path as string."""
    relpath = ""
    for i in range(len(path) - 1):
        part = os.path.relpath(path[i+1].name, path[i].name)
        relpath = f"{relpath}/{part}"
    relpath = relpath[1:]  # Remove header '/'
    return relpath
