# -*- coding: utf-8 -*-

from .context import rearchiving
from rearchiving import Dataset, create_archive, dataset
from rearchiving.paths import resolve_sym

import unittest
import shutil
from pathlib import Path
from tarfile import TarFile, TarInfo
import tarfile
from subprocess import run, PIPE
import filecmp
import json
import itertools

import os

TESTDIR = os.path.abspath(os.path.dirname(__file__))
TESTDATADIR = os.path.join(TESTDIR, "data/")
TESTDATASET=os.path.abspath(os.path.join(os.path.dirname(__file__), 'data/8888_internal_test_verysmall.tar'))
TESTRECORDDSET=os.path.join(TESTDATADIR, "6952.json")

def validate_members(d1, d2):
    filenames = [x.name for x in d1.members]
    foundfiles = [x.name for x in d2.members]
    assert len(set(filenames).difference(foundfiles)) == 0, f"Found unexpected files:\n{set(filenames).difference(foundfiles)}"
    assert set(filenames).issuperset(foundfiles), f"Found unexpected files:\n{set(filenames).difference(foundfiles)}"
    assert len(d1.members) == len(d2.members), "Datasets missing have same amount of files"
    d1fileinfo = [(f.name, f.size) for f in d1.members if f.isfile()]
    d2fileinfo = [(f.name, f.size) for f in d2.members if f.isfile()]
    assert set(d1fileinfo) == set(d2fileinfo)
    # assert all(i in d1fileinfo for i in d2fileinfo)

def validate_info(d1, d2):
    d1info = [x.get_info() for x in d1.members]
    d2info = [x.get_info() for x in d2.members]
    d1names = [f["name"] for f in d1info]
    d2names = [f["name"] for f in d2info]
    assert set(d1names) == set(d2names)
    assert len(d1.members) == len(d2.members), "Datasets don't have same amount of files."
    d1fileinfo = [(f["name"], f["size"]) for f in d1info if f["type"] == "REGTYPE"]
    d2fileinfo = [(f["name"], f["size"]) for f in d2info if f["type"] == "REGTYPE"]
    assert set(d1fileinfo) == set(d2fileinfo)

def validate_manifest(d1, d2):
    d1manifest = sorted(d1.extract_manifest(), key=lambda x: x["name"])
    d2manifest = sorted(d2.extract_manifest(), key=lambda x: x["name"])
    assert len(d1.members) == len(d2.members), "New archives don't have same amount of files."
    d1md5s = [(x["name"], x["md5"]) for x in d1manifest if x["type"] == "REGTYPE"]
    d2md5s = [(x["name"], x["md5"]) for x in d2manifest if x["type"] == "REGTYPE"]
    assert d1md5s == d2md5s

def test_read_archive():
    d = Dataset(TESTDATASET)
    contents = run(["tar", "-tf", TESTDATASET], check=True, stdout=PIPE).stdout
    contents = contents.decode()
    filenames = [Path(x) for x in contents.split("\n")[:-1]] # remove trailing newline
    print(len(filenames))
    print(type(filenames[0]))
    print(d)
    print(type(d.members[0].name))
    foundfiles = set(Path(x.name) for x in d.members)
    assert set(filenames).issuperset(foundfiles), f"Found unexpected files:\n{set(filenames).difference(foundfiles)}"

def test_read_dir(tmpdir):
    d1 = Dataset(TESTDATASET)
    with TarFile.open(TESTDATASET) as t:
        t.extractall(tmpdir)
    d2 = Dataset(tmpdir.join(d1.name))
    files1 = set([m.name for m in d1.members])
    files2 = set([m.name for m in d2.members])
    assert files1 == files2
    validate_info(d1, d2)
    validate_manifest(d1, d2)
    validate_members(d1, d2)

def test_multiple_manifests(tmpdir):
    # Setup
    d_orig = Dataset(TESTDATASET)
    reformat_dir = tmpdir.mkdir("8888_reformated")
    d_reformat = d_orig.reformat_dataset(reformat_dir)
    extracted_dir = tmpdir.mkdir("8888_extracted")
    # TarFile.open(TESTDATASET) as t:
    #     t.extractall(extracted_dir)
    for a in d_reformat.archives:
        a.extractall(extracted_dir)
    d_extract = Dataset(extracted_dir.join(d_orig.name))
    orig_manifest = d_orig.extract_manifest()
    reformat_manifests = d_reformat.extract_manifests()
    reformat_manifest = list(itertools.chain.from_iterable(reformat_manifests.values()))
    extract_manifest = d_extract.extract_manifest()
    # Files should be unique to
    orig_md5s = [(x["name"], x["md5"]) for x in orig_manifest if x["type"] == "REGTYPE"]
    reformat_md5s = [(x["name"], x["md5"]) for x in reformat_manifest if x["type"] == "REGTYPE"]
    extract_md5s = [(x["name"], x["md5"]) for x in extract_manifest if x["type"] == "REGTYPE"]
    assert set(orig_md5s) == set(reformat_md5s)
    assert set(extract_md5s) == set(reformat_md5s)
    assert set(orig_md5s) == set(extract_md5s)

def test_read_record():
    d = Dataset()
    with open(TESTRECORDDSET, "r") as f:
        dicts = json.load(f)
    d.members = [dataset.RecordMember(d, record) for record in dicts]
    assert d.findname("6952/source/processed").issym()
    symlink = d.findname("6952/source/normalized/normalized_expression.txt")
    origlinkname = symlink.linkname # Points to '../processed/normalized/gene_count_frags_TMM_RPKM_log2.txt'
    d.resolve_symlinks() # Should be '../processed.hg19.20160524-021816/normalized/gene_count_frags_TMM_RPKM_log2.txt'
    assert symlink.linkname != origlinkname

def test_reformat_archive(tmpdir):
    newdir = tmpdir.mkdir("8888_reformated")
    d1 = Dataset(TESTDATASET)
    d2 = d1.reformat_dataset(newdir)
    validate_members(d1, d2)
    validate_info(d1, d2)
    validate_manifest(d1, d2)

def test_reformatted_unpacked(tmpdir):
    newdir = tmpdir.mkdir("8888_reformated")
    unpackeddir1 = tmpdir.mkdir("unpacked1")
    unpackeddir2 = tmpdir.mkdir("unpacked2")
    d = Dataset(TESTDATASET)
    d1 = d.reformat_dataset(newdir)
    for a in d.archives:
        a.extractall(unpackeddir1)
    for a in d1.archives:
        a.extractall(unpackeddir2)
    # comp = filecmp.dircmp(unpackeddir1, unpackeddir2)
    match, mismatch, errors = filecmp.cmpfiles(unpackeddir1, unpackeddir2, [x.name for x in d.members if not (x.isdir() | x.islnk() | x.issym())])
    assert len(mismatch) == 0, f"Different files:\n{mismatch}" # Why does this not like the directories and links?
    assert len(errors) == 0, f"Error files:\n{mismatch}"

def test_symlinkres_tar(tmpdir):
    newdir = tmpdir.mkdir("8888_reformated")
    d = Dataset(TESTDATASET)
    names = [x.name for x in d.members]
    symlinks = list(d.collect_symlinks())
    assert not all([resolve_sym(x.name, x.linkname) in names for x in symlinks])
    d.resolve_symlinks()
    assert all([resolve_sym(x.name, x.linkname) in names for x in symlinks])
    d1 = d.reformat_dataset(tmpdir)
    for a in d1.archives:
        a.extractall(tmpdir)
    unpacked_links = list(d1.collect_symlinks())
    assert set(x.name for x in symlinks) == set(x.name for x in unpacked_links)
    for lnk in unpacked_links:
        d1.findname(resolve_sym(lnk.name, lnk.linkname))

def test_symlinkres_fs(tmpdir):
    newdir = tmpdir.mkdir("8888")
    d_tar = Dataset(TESTDATASET)
    d_tar.archives[0].extractall(newdir)
    d = Dataset(newdir)
    names = [x.name for x in d.members]
    symlinks = list(d.collect_symlinks())
    assert not all([resolve_sym(x.name, x.linkname) in names for x in symlinks])
    d.resolve_symlinks()
    assert all([resolve_sym(x.name, x.linkname) in names for x in symlinks])

def test_ignore_certain_symlinks(tmpdir):
    test_dir = Path(tmpdir.mkdir("test_dir"))
    testdataset = test_dir.joinpath(Path(TESTDATASET).name)
    shutil.copy(TESTDATASET, testdataset)
    # okay tar info
    ti = TarInfo("8888_internal_test_verysmall/source/raw/test_link.fastq")
    ti.type = tarfile.SYMTYPE
    ti.linkname = "./A1.R1.fastq"
    d_tar = TarFile.open(testdataset, "a")
    d_tar.addfile(ti)
    d_tar.close()
    dset = Dataset(testdataset)
    dset.validate_symlinks() # Would fail if dataset not valid.
    dset.resolve_symlinks()

def test_modes_plus_disk_to_archive(tmpdir):
    d_orig = Dataset(TESTDATASET)
    extract_dir = tmpdir.mkdir("extracted")
    reformat_dir = tmpdir.mkdir("reformatted")
    with TarFile.open(TESTDATASET) as t:
        t.extractall(extract_dir)
    d_extract = Dataset(extract_dir.join(d_orig.name))
    for m in d_extract.members:
        if m.isdir():
            m.mode = 0o775
        elif m.isfile():
            m.mode = 0o664
    d_reformat = d_extract.reformat_dataset(reformat_dir)
    # reformat_modes = [(m.name, m.mode) for m in d_reformat.members]
    reformat_modes = [(m.name, int(oct(m.mode)[-3:])) for m in d_reformat.members]
    extract_modes = [(m.name, int(oct(m.mode)[-3:])) for m in d_extract.members]
    assert set(reformat_modes) == set(extract_modes)
    validate_manifest(d_extract, d_reformat)
    validate_info(d_extract, d_reformat)
    validate_members(d_extract, d_reformat)
# def test_match_structure(tmpdir):

#
# if __name__ == '__main__':
#     unittest.main()
